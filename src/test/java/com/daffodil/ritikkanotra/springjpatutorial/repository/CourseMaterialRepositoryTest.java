package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.Course;
import com.daffodil.ritikkanotra.springjpatutorial.entity.CourseMaterial;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseMaterialRepositoryTest {

    @Autowired
    private CourseMaterialRepository courseMaterialRepository;

    @Test
    public void saveCourseMaterial() {

        Course course = Course.builder()
                .courseTitle("Spring")
                .credit(8)
                .build();

        CourseMaterial courseMaterial = CourseMaterial.builder()
                .url("www.course-spring.com")
                .course(course)
                .build();

        courseMaterialRepository.save(courseMaterial);
    }

    @Test
    public void printListOfCourseMaterials() {
        List<CourseMaterial> courseMaterials = courseMaterialRepository.findAll();
        System.out.println(courseMaterials);
    }

}
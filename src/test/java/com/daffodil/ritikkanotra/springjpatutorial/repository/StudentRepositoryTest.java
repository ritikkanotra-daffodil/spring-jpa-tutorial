package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.Guardian;
import com.daffodil.ritikkanotra.springjpatutorial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class StudentRepositoryTest {

    @Autowired
    StudentRepository studentRepository;

    @Test
    public void saveStudent() {
        Student student = Student.builder()
                .emailId("ritik@mail.com")
                .firstName("Ritik")
                .lastName("Kanotra")
//                .guardianName("Guard")
//                .guardianMobile("999999999")
//                .guardianEmail("guard@mail.com")
                .build();

        studentRepository.save(student);
    }

    @Test
    public void saveStudentWithGuardian() {

        Guardian guardian = Guardian.builder()
                .name("shan guard")
                .email("shan_guard@mail.com")
                .mobile("0009990001")
                .build();

        Student student = Student.builder()
                .firstName("Shantanu")
                .lastName("Gupta")
                .emailId("shan1@mail.com")
                .guardian(guardian)
                .build();

        studentRepository.save(student);
    }

    @Test
    public void printAllStudent() {
        List<Student> students = studentRepository.findAll();
        System.out.println(students);
    }

    @Test
    public void printStudentByFirstName() {
        List<Student> students = studentRepository.findByFirstName("Shantanu");
        System.out.println(students);
    }

    @Test
    public void printStudentByFirstNameContaining() {
        List<Student> students = studentRepository.findByFirstNameContaining("sh");
        System.out.println(students);
    }

    @Test
    public void printStudentByLastName() {
        List<Student> students = studentRepository.findByLastName("Gupta");
        System.out.println(students);
    }

    @Test
    public void printStudentByGuardianName() {
        List<Student> students = studentRepository.findByGuardianName("shan guard");
        System.out.println(students);
    }

    @Test
    public void printStudentByEmailAddress() {
        String emailId = "ritik@mail.com";
        Student student = studentRepository.getStudentByEmailAddress(emailId);
        System.out.println(student);
    }

    @Test
    public void printStudentFirstnameByEmailAddress() {
        String emailId = "ritik@mail.com";
        String firstName = studentRepository.getStudentFirstNameByEmailAddress(emailId);
        System.out.println(firstName);
    }

    @Test
    public void printStudentByEmailAddressNative() {
        String emailId = "ritik@mail.com";
        Student student = studentRepository.getStudentByEmailAddressNative(emailId);
        System.out.println(student);
    }

    @Test
    public void printStudentByEmailAddressNativeNamedPraam() {
        String emailId = "ritik@mail.com";
        Student student = studentRepository.getStudentByEmailAddressNativeNameParam(emailId);
        System.out.println(student);
    }

    @Test
    public void updateStudentNameByEmailId() {
        studentRepository.updateStudentNameByEmailId("RK", "ritik@mail.com");
    }

}
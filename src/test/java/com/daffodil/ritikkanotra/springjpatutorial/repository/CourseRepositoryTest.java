package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.Course;
import com.daffodil.ritikkanotra.springjpatutorial.entity.Student;
import com.daffodil.ritikkanotra.springjpatutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

//    @Test
//    public void saveCourse() {
//        Course course = Course.builder()
//                .courseTitle("")
//                .credit(8)
//                .build();
//
//        courseRepository.save(course);
//    }

    @Test
    public void printCourses() {
        List<Course> courses = courseRepository.findAll();
        System.out.println(courses);
    }

    @Test
    public void saveCourseWithTeacher() {

        Teacher teacher = Teacher.builder()
                .firstName("Pythoni")
                .lastName("Lal")
                .build();

        Course course = Course.builder()
                .courseTitle("Python")
                .credit(5)
                .teacher(teacher)
                .build();

        courseRepository.save(course);
    }

    @Test
    public void saveCourseWithStudentAndTeacher() {
        Teacher teacher = Teacher.builder()
                .firstName("Geeta")
                .lastName("Farm")
                .build();

        Student student = Student.builder()
                .firstName("Abhishek")
                .lastName("Singh")
                .emailId("abhi@mail.com")
                .build();

        Course course = Course.builder()
                .courseTitle("AI")
                .credit(12)
                .teacher(teacher)
                .students(List.of(student))
                .build();

        courseRepository.save(course);
    }

}
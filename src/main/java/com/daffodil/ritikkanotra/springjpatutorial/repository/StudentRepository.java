package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.Guardian;
import com.daffodil.ritikkanotra.springjpatutorial.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

        public List<Student> findByFirstName(String firstName);
        public List<Student> findByFirstNameContaining(String name);
        public List<Student> findByLastName(String lastName);
        public List<Student> findByGuardianName(String guardianName);
        public Student findByFirstNameAndLastName(String firstName, String lastName);

        //JPQL
        @Query("Select s from Student s where s.emailId = ?1")  //Based on classes you have created
        public Student getStudentByEmailAddress(String emailId);

        @Query("Select s.firstName from Student s where s.emailId = ?1")
        public String getStudentFirstNameByEmailAddress(String emailId);

        //Native Query
        @Query(
                value = "select * from tbl_student s where s.email_address = ?1",
                nativeQuery = true

        )
        public Student getStudentByEmailAddressNative(String emailId);


        //Native Query
        @Query(
                value = "select * from tbl_student s where s.email_address = :emailId",
                nativeQuery = true

        )
        public Student getStudentByEmailAddressNativeNameParam(
                @Param("emailId") String emailId
        );

        @Modifying
        @Transactional //Transaction (is generally added in service layer) (can be implemented at method or class level)
        @Query(
                value = "update tbl_student set first_name = ?1 where email_address = ?2",
                nativeQuery = true
        )
        public int updateStudentNameByEmailId(String firstName, String emailId);

}
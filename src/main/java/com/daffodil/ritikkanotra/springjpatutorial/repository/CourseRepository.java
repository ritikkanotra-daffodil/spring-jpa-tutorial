package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.Course;
import org.hibernate.usertype.CompositeUserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
}

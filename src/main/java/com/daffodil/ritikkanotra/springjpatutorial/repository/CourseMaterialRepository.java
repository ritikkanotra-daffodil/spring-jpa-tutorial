package com.daffodil.ritikkanotra.springjpatutorial.repository;

import com.daffodil.ritikkanotra.springjpatutorial.entity.CourseMaterial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseMaterialRepository extends JpaRepository<CourseMaterial, Long> {
}
